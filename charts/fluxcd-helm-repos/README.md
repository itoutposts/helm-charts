*This helm chart will deploy HelmRepositories resources for the most useful registries which can be used by FluxCD*


Add helm repository
```
flux create source helm itoutposts \
    --url=https://gitlab.com/api/v4/projects/41110054/packages/helm/stable \
    --interval=60m
```
Install Helm chart
```
flux create hr fluxcd-helm-repos \
    --target-namespace=flux-system \
    --create-target-namespace=true \
    --source=HelmRepository/itoutposts \
    --chart=fluxcd-helm-repos
```
 OR apply via kubectl or Git
```
---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: fluxcd-helm-repos
  namespace: flux-system
spec:
  targetNamespace: flux-system
  upgrade:
    preserveValues: false
  install:
    createNamespace: true
  interval: 10m
  chart:
    spec:
      chart: fluxcd-helm-repos
      sourceRef:
        kind: HelmRepository
        name: itoutposts
        namespace: flux-system
      interval: 1m
  values:
    helmRepos:
      installAll: false
```

You can enable or disable repositorie by switching values 

```agsl
helmRepos:
  repos:
    jetstack: false
    bitnami: true
    ingressNginx: false
    prometheusCommunity: false

```